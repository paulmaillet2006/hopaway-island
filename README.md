Hopaway Island est un hub de mini-jeux educatifs qui a pour but de vous permettre de travailler votre cerveau en vous amusant.

Pour découvrir Hopaway Island, ouvrez le fichier "Lien de téléchargement" puis cliquer sur le lien qui vous redirigera sur la page du jeu. Installer la version qui correspond à votre système d'exploitation puis décompresser le dossier avant de lancer l'application nommée "Hopaway Island V1.1.1".

Si vous voulez modifier le jeu afin de rajouter du contenu, veuillez d'abord installer "Godot Engine" afin de pouvoir placer les données du dossier "sources" dans votre nouveau projet Godot.

Vos recommendations sont les bienvenues, quand vous aurez terminé d'apprendre, répondez au Google Forms en lien dans le dossier "Hopaway Island".